using UnityEngine;

public class State : MonoBehaviour
{
    public virtual void OnEnter() { }
    public virtual void OnUpdate() { }
    public virtual void OnExit() { }
}
