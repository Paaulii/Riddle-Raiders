using System;

[Serializable]
public enum EffectType
{
    Blood,
    Smoke,
    Skull,
    PickupStar,
    Firework,
    FireworkShoot,
    LaserEffect
}