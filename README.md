# 🎮 Riddle Raiders ♟

## A puzzle coop game, two players need to figure out how to pass levels using different size abilities.
![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/0e02a8e9-4533-483a-ae6e-4e8b04174ebd)


Level 1                    |  Level 2                  |  Level 3
:-------------------------:|:-------------------------:|:-------------------------:
![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/47931b5e-3ee2-4431-925f-b903d2af9d41)|  ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/de2dc93e-0081-40bc-a249-89ee317fe12d)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/c545fc23-5d95-4c3e-a8c8-94bd8fbfbe9d)


Level 4                    |  Level 5                  |  Level 6
:-------------------------:|:-------------------------:|:-------------------------:
![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/d4ef0662-57f7-4c9a-9784-fb4343d3572c)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/990bb2ed-0cd2-4775-891d-7dea9ff23a87)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/7373d5a6-65e2-4a87-9de7-d1d558d2998d)



Level 7                    |  Level 8                  |  Level 9
:-------------------------:|:-------------------------:|:-------------------------:
![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/0af09625-b39b-4630-8fe0-c09ca289d43e)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/d53760e7-2b49-4ce9-8586-a5ba05e5875e)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/ee76eef4-8125-40b5-969e-69e7ea64b522)


Level 10                    |  Level 11                  |  Level 12
:-------------------------:|:-------------------------:|:-------------------------:
![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/ea7d3fc8-2b22-4b8b-8266-1f0540583e22)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/dd689820-622f-499f-b9da-4ef4ec6a8c49)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/ff2718ed-187a-43ff-9304-4246dca712ba)



Level 13                    |  Level 14                  |  Level 15
:-------------------------:|:-------------------------:|:-------------------------:
![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/b1a96d18-63a0-48a6-8965-4342df36caa2)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/08f7480e-a866-4c48-80ea-e77dae7c4f9e)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/64116006-026b-4395-99e2-d171a9777c8e)


Level selection             |  Settings                  |  Level complete
:-------------------------:|:-------------------------:|:-------------------------:
![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/9530f961-282e-451d-9fd1-d7a1f6177df6)| ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/f7466810-a134-4f29-8f4e-3865e5db3927) | ![image](https://github.com/Paaulii/Riddle-Raiders/assets/71827160/183d82f9-fa7f-4d01-8d73-30f7c69a8eea)


### Used assets in game:


#### Music / Sounds:

- With Love From Vertex Studio (16) by Vertex Studio
https://soundcloud.com/en7ity/with-love-from-vertex-15?in=en7ity/sets/vertex-studio-music 

- With Love From Vertex Studio (4) by Vertex Studio
https://soundcloud.com/en7ity/with-love-from-vertex-27?in=en7ity/sets/vertex-studio-music 

- With Love From Vertex Studio (11) by Vertex Studio
https://soundcloud.com/en7ity/with-love-from-vertex-20?in=en7ity/sets/vertex-studio-music

- With Love From Vertex Studio (25) by Vertex Studio
https://soundcloud.com/en7ity/with-love-from-vertex-6?in=en7ity/sets/vertex-studio-music

- Tap Notification - Sound Effect by Luca Di Alessandro from Pixabay
https://pixabay.com/sound-effects/search/tap/

- Cartoon jump - Sound Effect from Pixabay
https://pixabay.com/sound-effects/search/jump/

- Cardboard_Small Impact_06 -  Sound Effect from Pixabay 
https://pixabay.com/sound-effects/search/cardboard/

- Movement Swipe Whoosh 4 - Sound Effect by floraphonic from Pixabay
https://pixabay.com/sound-effects/search/swipe/?pagi=2

- Swing Whoosh 5 - floraphonic - Sound Effect by floraphonic from Pixabay 
https://pixabay.com/sound-effects/search/whoosh/

- Sad game over trombone by Mixkit licensed Mixkit License. 
https://mixkit.co/free-sound-effects/game-over/

- Game level complete by Mixkit licensed Mixkit License. 
https://mixkit.co/free-sound-effects/game-over/

- Short laser gun shot by Mixkit licensed Mixkit License. 
https://mixkit.co/free-sound-effects/laser/

- Cinematic transition wind swoosh by Mixkit licensed Mixkit License. 
https://mixkit.co/free-sound-effects/wind/

- On or off light switch tap by Mixkit licensed Mixkit License.
https://mixkit.co/free-sound-effects/tap/

- UI Sound Effects Collection Pack 2: Buttons by Nectar Sonic Lab.
https://assetstore.unity.com/packages/audio/sound-fx/ui-sound-effects-collection-pack-2-buttons-27803#content


#### GUI assets:
- 2d Casual UI HD by MiMU STUDIO - https://assetstore.unity.com/packages/2d/gui/icons/2d-casual-ui-hd-82080#asset_quality
 
 - Simple Gems Ultimate Animated Customizable Pack  by AurynSky - https://assetstore.unity.com/packages/3d/props/simple-gems-ultimate-animated-customizable-pack-73764#reviews

- Cartoon FX Remaster Free by Jean Moreno - https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-remaster-free-109565

- Pixel Art Platformer - Village Props by Cainos - https://assetstore.unity.com/packages/2d/environments/pixel-art-platformer-village-props-166114

- Puzzle stage & settings GUI Pack by Chocoball - https://assetstore.unity.com/packages/2d/gui/puzzle-stage-settings-gui-pack-147389

#### Fonts:
- Puffy by Stefie Justprince licensed OFL:
https://fontesk.com/puffy-font/

- Catfiles by Syafrizal a.k.a. Khurasan licensed OFL:
https://fontesk.com/catfiles-font/

- Sohungry by Syafrizal a.k.a. Khurasan licensed OFL:
https://fontesk.com/sohungry-font/


#### Plugins:
- DOTween by Demigiant 
https://github.com/Demigiant/dotween
